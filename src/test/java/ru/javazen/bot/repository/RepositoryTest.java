package ru.javazen.bot.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.javazen.bot.model.ClassDefinition;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RepositoryTestConfiguration.class })
@Transactional
public class RepositoryTest {

    @Autowired
    private ClassDefinitionRepository classDefinitionRepository;

    @Before
    public void setUp() {

        ClassDefinition classDefinition = new ClassDefinition();
        classDefinition.setName("java.lang.String");

        classDefinitionRepository.save(classDefinition);
    }

    @Test
    public void testFindByName() {
        ClassDefinition classDefinition = classDefinitionRepository.findByName("java.lang.String");
        assertNotNull(classDefinition);
    }

    @After
    public void clean() {
        classDefinitionRepository.deleteAll();
    }
}
