package ru.javazen.bot.handler;


import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.AbsSender;

public interface UpdateHandler {
    boolean handle(Update update, AbsSender sender);
}
