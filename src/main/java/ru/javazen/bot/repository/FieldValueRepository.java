package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.FieldValue;

public interface FieldValueRepository extends CrudRepository<FieldValue, Long> {

    Iterable<FieldValue> findByClassFieldId(Long id);

    Iterable<FieldValue> findByClassInstanceId(Long id);

}
