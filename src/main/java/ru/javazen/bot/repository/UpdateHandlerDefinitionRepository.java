package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.UpdateHandlerDefinition;

public interface UpdateHandlerDefinitionRepository extends CrudRepository<UpdateHandlerDefinition, Long> {
    
}
