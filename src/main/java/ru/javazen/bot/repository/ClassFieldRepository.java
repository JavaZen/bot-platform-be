package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.ClassField;

import java.util.List;

public interface ClassFieldRepository extends CrudRepository<ClassField, Long> {

    List<ClassField> findByName(String name);

    Iterable<ClassField> findByClassDefinitionId(Long id);
}
