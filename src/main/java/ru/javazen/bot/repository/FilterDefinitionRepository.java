package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.FilterDefinition;

public interface FilterDefinitionRepository extends CrudRepository<FilterDefinition, Long> {

}
