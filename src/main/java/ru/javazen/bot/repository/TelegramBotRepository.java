package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.TelegramBot;

public interface TelegramBotRepository extends CrudRepository<TelegramBot, Long> {

    TelegramBot findByName(String name);
}
