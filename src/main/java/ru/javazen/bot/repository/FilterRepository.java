package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.Filter;

public interface FilterRepository extends CrudRepository<Filter, Long> {

    public Iterable<Filter> findByUpdateHandlerId(Long id);
}
