package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.UpdateHandler;

public interface UpdateHandlerRepository extends CrudRepository<UpdateHandler, Long> {

    Iterable<UpdateHandler> findByTelegramBotId(Long id);
}
