package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.ClassInstance;

public interface ClassInstanceRepository extends CrudRepository<ClassInstance, Long> {
}
