package ru.javazen.bot.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.bot.model.ClassDefinition;

public interface ClassDefinitionRepository extends CrudRepository<ClassDefinition, Long> {

    ClassDefinition findByName(String name);
}
