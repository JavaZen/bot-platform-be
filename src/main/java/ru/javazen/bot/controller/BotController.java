package ru.javazen.bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.javazen.bot.model.TelegramBot;
import ru.javazen.bot.repository.TelegramBotRepository;

@RestController
@RequestMapping("bot")
public class BotController extends CrudController<TelegramBot, Long> {

    @Autowired
    private TelegramBotRepository telegramBotRepository;

    @Override
    public TelegramBotRepository getRepository() {
        return telegramBotRepository;
    }

}
