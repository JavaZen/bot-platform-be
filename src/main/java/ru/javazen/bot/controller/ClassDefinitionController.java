package ru.javazen.bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.javazen.bot.model.ClassDefinition;
import ru.javazen.bot.repository.ClassDefinitionRepository;

@RestController
@RequestMapping("class/definition")
public class ClassDefinitionController extends CrudController<ClassDefinition, Long> {

    @Autowired
    private ClassDefinitionRepository classDefinitionRepository;

    @Override
    public ClassDefinitionRepository getRepository() {
        return classDefinitionRepository;
    }

}