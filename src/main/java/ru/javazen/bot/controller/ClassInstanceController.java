package ru.javazen.bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.javazen.bot.model.ClassInstance;
import ru.javazen.bot.repository.ClassInstanceRepository;

@RestController
@RequestMapping("class/instance")
public class ClassInstanceController extends CrudController<ClassInstance, Long> {

    @Autowired
    private ClassInstanceRepository classInstanceRepository;

    @Override
    public ClassInstanceRepository getRepository() {
        return classInstanceRepository;
    }

}
