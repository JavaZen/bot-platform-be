package ru.javazen.bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;
import ru.javazen.bot.model.Filter;
import ru.javazen.bot.repository.FilterRepository;

@RestController
@RequestMapping("bot/filter")
public class FilterController extends CrudController<Filter, Long> {

    @Autowired
    private FilterRepository filterRepository;

    @Override
    public FilterRepository getRepository() {
        return filterRepository;
    }

    @RequestMapping(path = "/get/{id}/handler", method = RequestMethod.POST)
    public Iterable<Filter> getByHandler(@PathVariable Long id) {
        return filterRepository.findByUpdateHandlerId(id);
    }

}
