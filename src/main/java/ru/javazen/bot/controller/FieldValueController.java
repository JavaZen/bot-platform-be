package ru.javazen.bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.javazen.bot.model.FieldValue;
import ru.javazen.bot.repository.FieldValueRepository;

@RestController
@RequestMapping("class/value")
public class FieldValueController extends CrudController<FieldValue, Long> {

    @Autowired
    private FieldValueRepository fieldValueRepository;

    @Override
    public FieldValueRepository getRepository() {
        return fieldValueRepository;
    }

    @RequestMapping("get/{id}/field")
    public Iterable<FieldValue> getValuesByField(@PathVariable Long id) {
        return fieldValueRepository.findByClassFieldId(id);
    }

    @RequestMapping("get/{id}/instance")
    public Iterable<FieldValue> getValuesByInstance(@PathVariable Long id) {
        return fieldValueRepository.findByClassInstanceId(id);
    }

}
