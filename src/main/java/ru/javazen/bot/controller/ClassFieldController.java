package ru.javazen.bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.javazen.bot.model.ClassField;
import ru.javazen.bot.repository.ClassFieldRepository;

@RestController
@RequestMapping("class/field")
public class ClassFieldController extends CrudController<ClassField, Long> {

    @Autowired
    private ClassFieldRepository classFieldRepository;

    @Override
    public ClassFieldRepository getRepository() {
        return classFieldRepository;
    }

    @RequestMapping("/get/{id}/definition")
    public Iterable<ClassField> getByClassDefinition(@PathVariable Long id) {
        return classFieldRepository.findByClassDefinitionId(id);
    }

}
