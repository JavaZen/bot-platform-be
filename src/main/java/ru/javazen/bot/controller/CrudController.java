package ru.javazen.bot.controller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;

public abstract class CrudController<T, ID extends Serializable> {

    public abstract CrudRepository<T, ID> getRepository();

    @RequestMapping("/{id}/get")
    public T get(@PathVariable ID id) {
        return getRepository().findOne(id);
    }

    @RequestMapping("/get/all")
    public Iterable<T> getAll() {
        return getRepository().findAll();
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public void add(@RequestBody T entity) {
        getRepository().save(entity);
    }

    @RequestMapping(path = "/modify", method = RequestMethod.POST)
    public void modify(@RequestBody T entity) {
        getRepository().save(entity);
    }

    @RequestMapping(path = "/{id}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable ID id) {
        getRepository().delete(id);
    }

}
