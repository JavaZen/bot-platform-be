package ru.javazen.bot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.javazen.bot.model.UpdateHandler;
import ru.javazen.bot.repository.UpdateHandlerRepository;

@RestController
@RequestMapping("bot/handler")
public class HandlerController extends CrudController<UpdateHandler, Long> {

    @Autowired
    private UpdateHandlerRepository updateHandlerRepository;

    @Override
    public UpdateHandlerRepository getRepository() {
        return updateHandlerRepository;
    }

    @RequestMapping(path = "/get/{id}/bot", method = RequestMethod.POST)
    public Iterable<UpdateHandler> getByBot(@PathVariable Long id) {
        return updateHandlerRepository.findByTelegramBotId(id);
    }

}
