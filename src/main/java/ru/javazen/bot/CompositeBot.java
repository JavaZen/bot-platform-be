package ru.javazen.bot;

import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import ru.javazen.bot.handler.UpdateHandler;

import java.util.List;

public class CompositeBot extends TelegramLongPollingBot {

    private String name;
    private String token;
    private List<UpdateHandler> updateHandlers;

    public CompositeBot(String name, String token, List<UpdateHandler> updateHandlers) {
        this.name = name;
        this.token = token;
        this.updateHandlers = updateHandlers;
    }

    @Override
    public void onUpdateReceived(Update update) {
        for (UpdateHandler updateHandler : updateHandlers) {
            updateHandler.handle(update, this);
        }
    }

    @Override
    public String getBotUsername() {
        return name;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    public List<UpdateHandler> getUpdateHandlers() {
        return updateHandlers;
    }

    public void setUpdateHandlers(List<UpdateHandler> updateHandlers) {
        this.updateHandlers = updateHandlers;
    }
}
