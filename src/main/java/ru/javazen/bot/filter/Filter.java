package ru.javazen.bot.filter;

import org.telegram.telegrambots.api.objects.Update;

public interface Filter {
    boolean check(Update update);
}
