package ru.javazen.bot.model;

import javax.persistence.*;
import java.util.Set;

/**
 * The class define some class definition in the system
 */
@Entity
@Table(name = "CLASS_DEFINITION")
public class ClassDefinition {

    @Id
    @Column(name = "CLASS_DEFINITION_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @OneToMany(mappedBy="classDefinition", fetch = FetchType.EAGER)
    private Set<ClassField> fields;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ClassField> getFields() {
        return fields;
    }

    public void setFields(Set<ClassField> fields) {
        this.fields = fields;
    }
}
