package ru.javazen.bot.model;

import javax.persistence.*;

@Entity
@Table(name = "FIELD_VALUE")
public class FieldValue {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "class_instance_id")
    private ClassInstance classInstance;

    @ManyToOne
    @JoinColumn(name = "class_field_id")
    private ClassField classField;

    @Column
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClassInstance getClassInstance() {
        return classInstance;
    }

    public void setClassInstance(ClassInstance classInstance) {
        this.classInstance = classInstance;
    }

    public ClassField getClassField() {
        return classField;
    }

    public void setClassField(ClassField classField) {
        this.classField = classField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
