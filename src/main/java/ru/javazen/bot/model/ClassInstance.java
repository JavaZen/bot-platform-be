package ru.javazen.bot.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Present some class instance in the system
 */
@Entity
@Table(name = "CLASS_INSTANCE")
public class ClassInstance {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "class_definition_id")
    private ClassDefinition classDefinition;

    @OneToMany(mappedBy="classInstance", fetch = FetchType.EAGER)
    private Set<FieldValue> fieldValues;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClassDefinition getClassDefinition() {
        return classDefinition;
    }

    public void setClassDefinition(ClassDefinition classDefinition) {
        this.classDefinition = classDefinition;
    }

    public Set<FieldValue> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(Set<FieldValue> fieldValues) {
        this.fieldValues = fieldValues;
    }
}
