package ru.javazen.bot.model;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "TELEGRAM_BOT")
public class TelegramBot {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String token;

    @Column
    private Boolean enabled;

    @OneToMany(mappedBy="telegramBot", fetch = FetchType.EAGER)
    private Set<UpdateHandler> updateHandlers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Set<UpdateHandler> getUpdateHandlers() {
        return updateHandlers;
    }

    public void setUpdateHandlers(Set<UpdateHandler> updateHandlers) {
        this.updateHandlers = updateHandlers;
    }
}
