package ru.javazen.bot.model;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "UPDATE_HANDLER")
public class UpdateHandler {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name = "update_handler_definition_id")
    private UpdateHandlerDefinition updateHandlerDefinition;

    @OneToOne
    @JoinColumn(name = "class_instance_id")
    private ClassInstance classInstance;

    @ManyToOne
    @JoinColumn(name = "telegram_bot_id")
    private TelegramBot telegramBot;

    @OneToMany(mappedBy="updateHandler", fetch = FetchType.EAGER)
    private Set<Filter> filters;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UpdateHandlerDefinition getUpdateHandlerDefinition() {
        return updateHandlerDefinition;
    }

    public void setUpdateHandlerDefinition(UpdateHandlerDefinition updateHandlerDefinition) {
        this.updateHandlerDefinition = updateHandlerDefinition;
    }

    public ClassInstance getClassInstance() {
        return classInstance;
    }

    public void setClassInstance(ClassInstance classInstance) {
        this.classInstance = classInstance;
    }

    public TelegramBot getTelegramBot() {
        return telegramBot;
    }

    public void setTelegramBot(TelegramBot telegramBot) {
        this.telegramBot = telegramBot;
    }

    public Set<Filter> getFilters() {
        return filters;
    }

    public void setFilters(Set<Filter> filters) {
        this.filters = filters;
    }
}
