package ru.javazen.bot.model;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "UPDATE_HANDLER_DEFINITION")
public class UpdateHandlerDefinition {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "update_handler_definition_id")
    private UpdateHandlerDefinition updateHandlerDefinition;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UpdateHandlerDefinition getUpdateHandlerDefinition() {
        return updateHandlerDefinition;
    }

    public void setUpdateHandlerDefinition(UpdateHandlerDefinition updateHandlerDefinition) {
        this.updateHandlerDefinition = updateHandlerDefinition;
    }
}
